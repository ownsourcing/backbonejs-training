#!/usr/bin/env bash
cp -r /vagrant/provision/etc/apt/* /etc/apt/
export DEBIAN_FRONTEND=noninteractive

apt-get -qq update
apt-get -qq upgrade
apt-get -qq install apache2
apt-get -qq install nodejs
apt-get -qq install vim-nox

cp -r /vagrant/provision/etc/* /etc/

a2dissite 000-default
a2ensite training

service apache2 restart
