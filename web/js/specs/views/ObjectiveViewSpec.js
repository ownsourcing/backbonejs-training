define([
    'app/models/ObjectiveModel',
    'app/views/ObjectiveView'
], function (ObjectiveModel, ObjectiveView) {
    describe('An ObjectiveView', function() {
        it('is defined', function () {
            expect(ObjectiveView).toBeDefined();
        });
        it('has a render method returning the instance', function () {
            var view = new ObjectiveView({ model: new ObjectiveModel() });
            expect(view.render).toBeDefined();
            expect(view.render()).toBe(view);
        });
        it('can generate HTML', function () {
            var model = new ObjectiveModel({
                title: 'This is an objective'
            });
            var view = new ObjectiveView({ model: model });
            var title = view.render().$('.objective-title').text();
            expect(title).toBe('This is an objective');
        });
        it('renders when its model changes', function () {
            spyOn(ObjectiveView.prototype, 'render');
            var model = new ObjectiveModel({ title: 'This is a step' });
            var view = new ObjectiveView({ model: model });
            model.set('title', 'This is another step');
            expect(view.render).toHaveBeenCalled();
        });
    });
});
