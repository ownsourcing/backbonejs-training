define([
    'app/models/StepModel',
    'app/views/StepView'
], function (StepModel, StepView) {
    describe('A StepView', function() {
        it('is defined', function () {
            expect(StepView).toBeDefined();
        });
        it('has a render method returning the instance', function () {
            var view = new StepView({ model: new StepModel() });
            expect(view.render).toBeDefined();
            expect(view.render()).toBe(view);
        });
        it('can generate HTML', function () {
            var model = new StepModel({ title: 'This is a step' });
            var view = new StepView({ model: model });
            var title = view.render().$('.step-title').text();
            expect(title).toBe('This is a step');
        });
        it('renders when its model changes', function () {
            spyOn(StepView.prototype, 'render');
            var model = new StepModel({ title: 'This is a step' });
            var view = new StepView({ model: model });
            model.set('title', 'This is another step');
            expect(view.render).toHaveBeenCalled();
        });
    });
});
