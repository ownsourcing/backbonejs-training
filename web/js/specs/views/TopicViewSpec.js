define([
    'app/models/TopicModel',
    'app/views/TopicView'
], function (TopicModel, TopicView) {
    describe('A TopicView', function() {
        it('is defined', function () {
            expect(TopicView).toBeDefined();
        });
        it('has a render method returning the instance', function () {
            var view = new TopicView({ model: new TopicModel() });
            expect(view.render).toBeDefined();
            expect(view.render()).toBe(view);
        });
        it('can generate HTML', function () {
            var model = new TopicModel({ title: 'This is a topic' });
            var view = new TopicView({ model: model });
            var title = view.render().$('.topic-title').text();
            expect(title).toBe('This is a topic');
        });
        it('renders when its model changes', function () {
            spyOn(TopicView.prototype, 'render');
            var model = new TopicModel({ title: 'This is a step' });
            var view = new TopicView({ model: model });
            model.set('title', 'This is another step');
            expect(view.render).toHaveBeenCalled();
        });
    });
});
