define([
    'app/collections/TopicCollection',
    'app/views/TopicCollectionView'
], function (TopicCollection, TopicCollectionView) {
    describe('A TopicCollectionView', function() {
        it('is defined', function () {
            expect(TopicCollectionView).toBeDefined();
        });
        it('has a render method returning the instance', function () {
            var view = new TopicCollectionView({
                collection: new TopicCollection()
            });
            expect(view.render).toBeDefined();
            expect(view.render()).toBe(view);
        });
        it('renders all topics', function () {
            var collection = new TopicCollection([
                { title: 'First topic'},
                { title: 'Second topic'}
            ]);
            var view = new TopicCollectionView({
                collection: collection
            });
            expect(view.render().$('section').length).toBe(2);
            // Assert rendering is idempotent.
            expect(view.render().$('section').length).toBe(2);
        });
        it('renders when the collection is reset', function () {
            spyOn(TopicCollectionView.prototype, 'render');
            var collection = new TopicCollection();
            var view = new TopicCollectionView({
                collection: collection
            });
            collection.reset([
                { title: 'First topic'},
                { title: 'Second topic'}
            ]);
            expect(view.render).toHaveBeenCalled();
        });
    });
});
