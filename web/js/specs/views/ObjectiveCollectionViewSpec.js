define([
    'app/collections/ObjectiveCollection',
    'app/views/ObjectiveCollectionView'
], function (ObjectiveCollection, ObjectiveCollectionView) {
    describe('An ObjectiveCollectionView', function() {
        it('is defined', function () {
            expect(ObjectiveCollectionView).toBeDefined();
        });
        it('has a render method returning the instance', function () {
            var view = new ObjectiveCollectionView({
                collection: new ObjectiveCollection()
            });
            expect(view.render).toBeDefined();
            expect(view.render()).toBe(view);
        });
        it('renders all objectives', function () {
            var collection = new ObjectiveCollection([
                { title: 'First objective'},
                { title: 'Second objective'}
            ]);
            var view = new ObjectiveCollectionView({
                collection: collection
            });
            expect(view.render().$('section').length).toBe(2);
            // Assert rendering is idempotent.
            expect(view.render().$('section').length).toBe(2);
        });
        it('renders when the collection is reset', function () {
            spyOn(ObjectiveCollectionView.prototype, 'render');
            var collection = new ObjectiveCollection();
            var view = new ObjectiveCollectionView({
                collection: collection
            });
            collection.reset([
                { title: 'First objective'},
                { title: 'Second objective'}
            ]);
            expect(view.render).toHaveBeenCalled();
        });
    });
});
