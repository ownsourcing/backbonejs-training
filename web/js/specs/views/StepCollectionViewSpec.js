define([
    'app/collections/StepCollection',
    'app/views/StepCollectionView'
], function (StepCollection, StepCollectionView) {
    describe('A StepCollectionView', function() {
        it('is defined', function () {
            expect(StepCollectionView).toBeDefined();
        });
        it('has a render method returning the instance', function () {
            var view = new StepCollectionView({
                collection: new StepCollection()
            });
            expect(view.render).toBeDefined();
            expect(view.render()).toBe(view);
        });
        it('renders all steps', function () {
            var collection = new StepCollection([
                { title: 'First step' },
                { title: 'Second step'}
            ]);
            var view = new StepCollectionView({
                collection: collection
            });
            expect(view.render().$('article').length).toBe(2);
            // Assert rendering is idempotent.
            expect(view.render().$('article').length).toBe(2);
        });
        it('renders when the collection is reset', function () {
            spyOn(StepCollectionView.prototype, 'render');
            var collection = new StepCollection();
            var view = new StepCollectionView({
                collection: collection
            });
            collection.reset([
                { title: 'First topic'},
                { title: 'Second topic'}
            ]);
            expect(view.render).toHaveBeenCalled();
        });
    });
});
