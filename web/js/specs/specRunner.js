require.config({
    baseUrl: 'js/lib',
    paths: {
        'app': '../app',
        'specs': '../specs',
        'jquery': 'jquery-2.1.0'
    },
    shim: {
        'jasmine': {
            exports: 'jasmine'
        },
        'jasmine-html': {
            deps: ['jasmine'],
            exports: 'jasmine'
        },
        'boot': {
            deps: ['jasmine', 'jasmine-html'],
            exports: 'jasmine'
        }
    }
});

var specs = [
    'specs/views/ObjectiveViewSpec',
    'specs/views/ObjectiveCollectionViewSpec',
    'specs/views/StepViewSpec',
    'specs/views/StepCollectionViewSpec',
    'specs/views/TopicViewSpec',
    'specs/views/TopicCollectionViewSpec',
    'specs/models/ObjectiveModelSpec',
    'specs/models/StepModelSpec',
    'specs/models/TopicModelSpec',
    'specs/collections/ObjectiveCollectionSpec',
    'specs/collections/StepCollectionSpec',
    'specs/collections/TopicCollectionSpec'
];

require(['boot'], function () {
    require(specs, function () {
        window.onload();
    });
});
