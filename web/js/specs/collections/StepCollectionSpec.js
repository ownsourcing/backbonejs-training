define([
    'app/collections/StepCollection',
], function (StepCollection) {
    describe('A StepCollection', function() {
        it('is defined', function () {
            expect(StepCollection).toBeDefined();
        });
    });
});
