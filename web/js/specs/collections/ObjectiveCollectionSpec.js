define([
    'app/collections/ObjectiveCollection',
], function (ObjectiveCollection) {
    describe('An ObjectiveCollection', function() {
        it('is defined', function () {
            expect(ObjectiveCollection).toBeDefined();
        });
    });
});
