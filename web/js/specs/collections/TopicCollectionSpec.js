define([
    'app/collections/TopicCollection',
], function (TopicCollection) {
    describe('A TopicCollection', function() {
        it('is defined', function () {
            expect(TopicCollection).toBeDefined();
        });
    });
});
