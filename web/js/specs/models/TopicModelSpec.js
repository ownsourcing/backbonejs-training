define([
    'app/models/TopicModel'
], function (TopicModel) {
    describe('A TopicModel', function() {
        it('is defined', function () {
            expect(TopicModel).toBeDefined();
        });
        it('has a title', function () {
            var model = new TopicModel();
            expect(model.has('title')).toBe(true);
        });
        it('has prerequisites', function () {
            var model = new TopicModel();
            expect(model.has('prerequisites')).toBe(true);
        });
    });
});
