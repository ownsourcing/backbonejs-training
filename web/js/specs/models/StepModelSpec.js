define([
    'app/models/StepModel'
], function (StepModel) {
    describe('A StepModel', function() {
        it('is defined', function () {
            expect(StepModel).toBeDefined();
        });
        it('has a title', function () {
            var model = new StepModel();
            expect(model.has('title')).toBe(true);
        });
        it('has a body', function () {
            var model = new StepModel();
            expect(model.has('body')).toBe(true);
        });
    });
});
