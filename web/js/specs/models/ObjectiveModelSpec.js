define([
    'app/models/ObjectiveModel'
], function (ObjectiveModel) {
    describe('An ObjectiveModel', function() {
        it('is defined', function () {
            expect(ObjectiveModel).toBeDefined();
        });
        it('has a title', function () {
            var model = new ObjectiveModel();
            expect(model.has('title')).toBe(true);
        });
    });
});
