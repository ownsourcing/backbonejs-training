define([
    'app/views/BaseCollectionView',
    'app/views/ObjectiveView'
], function (BaseCollectionView, ObjectiveView) {
    var ObjectiveCollectionView = BaseCollectionView.extend({
        render: function () {
            return this.renderModels(ObjectiveView);
        },
    });
    return ObjectiveCollectionView;
});
