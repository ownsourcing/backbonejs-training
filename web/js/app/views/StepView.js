define([
    'twig',
    'app/views/BaseView',
    'text!app/templates/step.twig'
], function (Twig, BaseView, template) {
    var TopicView = BaseView.extend({
        template: Twig.twig({ data: template }),
    });
    return TopicView;
});
