define([
    'twig',
    'app/views/BaseView',
    'text!app/templates/objective.twig'
], function (Twig, BaseView, template) {
    var ObjectiveView = BaseView.extend({
        template: Twig.twig({ data: template }),
    });
    return ObjectiveView;
});
