define([
    'app/views/BaseCollectionView',
    'app/views/TopicView'
], function (BaseCollectionView, TopicView) {
    var TopicCollectionView = BaseCollectionView.extend({
        render: function () {
            return this.renderModels(TopicView);
        },
    });
    return TopicCollectionView;
});
