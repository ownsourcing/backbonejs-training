define([
    'backbone',
], function (Backbone) {
    var BasicView = Backbone.View.extend({
        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },
        render: function () {
            this.$el.html(this.template.render(this.model.toJSON()));
            return this;
        }
    });
    return BasicView;
});
