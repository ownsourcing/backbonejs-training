define([
    'backbone',
    'underscore'
], function (Backbone, _) {
    var BaseCollectionView = Backbone.View.extend({
        initialize: function () {
            this.listenTo(this.collection, 'reset', this.render);
        },
        renderModels: function (viewClass) {
            this.$el.empty();
            this.collection.each(_.bind(function (model) {
                var view = new viewClass({ model: model });
                this.$el.append(view.render().el);
            }, this));
            return this;
        }
    });
    return BaseCollectionView;
});
