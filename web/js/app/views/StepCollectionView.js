define([
    'app/views/BaseCollectionView',
    'app/views/StepView'
], function (BaseCollectionView, StepView) {
    var StepCollectionView = BaseCollectionView.extend({
        render: function () {
            return this.renderModels(StepView);
        },
    });
    return StepCollectionView;
});
