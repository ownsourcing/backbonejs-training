define([
    'backbone'
], function (Backbone) {
    var StepModel = Backbone.Model.extend({
        defaults: {
            // A descriptive title
            title: '',
            // Hands on instructions
            body: '',
        }
    });
    return StepModel;
});
