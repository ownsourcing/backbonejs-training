define([
    'backbone'
], function (Backbone) {
    var TopicModel = Backbone.Model.extend({
        defaults: {
            // A descriptive title
            title: '',
            // A list of prerequisites
            prerequisites: [],
        }
    });
    return TopicModel;
});
