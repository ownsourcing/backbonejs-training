define([
    'backbone'
], function (Backbone) {
    var ObjectiveModel = Backbone.Model.extend({
        defaults: {
            // A descriptive title
            title: '',
        }
    });
    return ObjectiveModel;
});
