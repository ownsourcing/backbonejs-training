define([
    'backbone',
    'app/models/ObjectiveModel'
], function (Backbone, ObjectiveModel) {
    var ObjectiveCollection = Backbone.Collection.extend({
        model: ObjectiveModel
    });
    return ObjectiveCollection;
});
