define([
    'backbone',
    'app/models/StepModel'
], function (Backbone, StepModel) {
    var StepCollection = Backbone.Collection.extend({
        model: StepModel
    });
    return StepCollection;
});
