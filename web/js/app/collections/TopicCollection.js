define([
    'backbone',
    'app/models/TopicModel'
], function (Backbone, TopicModel) {
    var TopicCollection = Backbone.Collection.extend({
        model: TopicModel
    });
    return TopicCollection;
});
