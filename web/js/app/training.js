require.config({
    // http://requirejs.org/docs/api.html#config-baseUrl
    baseUrl: 'js/lib',
    // http://requirejs.org/docs/api.html#config-paths
    paths: {
        'app': '../app',
        // Centralize knowledge about version of jQuery.
        'jquery': 'jquery-2.1.0'
    },
    // http://requirejs.org/docs/api.html#config-map
    map: {
    },
    // http://requirejs.org/docs/api.html#config-shim
    shim: {
    }
});

define([
    '!domReady',
], function () {
    alert('Hello world!');
});
